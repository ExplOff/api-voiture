import http.client
import json
import concurrent.futures

def send_request(i):
    conn = http.client.HTTPSConnection("webapp-cars-gpatarin.cloud.okteto.net")
    payload = json.dumps({
      "manufacturer": "patate",
      "model": "patate",
      "build": 1
    })
    headers = {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    }
    conn.request("POST", "/cars", payload, headers)
    res = conn.getresponse()
    data = res.read()
    print(data.decode("utf-8"))

if __name__ == "__main__":
    num_requests = 100000
    num_threads = 10  # Nombre de threads simultanés
    with concurrent.futures.ThreadPoolExecutor(max_workers=num_threads) as executor:
        for i in range(num_requests):
            executor.submit(send_request, i)


# import http.client
# import json
# import concurrent.futures

# conn = http.client.HTTPSConnection("app-cars-api-raphaelcpp.cloud.okteto.net")
# payload = json.dumps({
#   "manufacturer": "patate",
#   "model": "patate",
#   "build": 1
# })
# headers = {
#   'Content-Type': 'application/json',
#   'Cache-Control': 'no-cache'
# }


# for i in range(100000):
#     conn.request("POST", "/cars", payload, headers)
#     res = conn.getresponse()
#     data = res.read()
#     print(data.decode("utf-8"))